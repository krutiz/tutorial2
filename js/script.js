'STRICT';

// for chapter 1
var commentaries = document.getElementsByClassName('clickToAppear'); // make an array with all the commentaries
var textToHighligh = document.getElementsByClassName('toBeHighlighted');

function changeText1() {
  document.getElementById('demo').innerHTML = 'I changed!'; // change the text inside the preview
  textToHighligh[0].innerHTML = 'I changed!'; // change the text inside html cell
  textToHighligh[0].style.backgroundColor = 'red'; // change the backgorund color of the changed text
  commentaries[0].style.opacity = 1; // change the first commentary to visible. The first is given by using the index 0, stated by 'commentaries[0]'
}

// for chapter 2
const colorBtn = document.querySelector('#btn1');
const bulbs = document.querySelectorAll('.bulb');

let running = false;

function getRandomColor() {
  let letters = "0123456789ABCDEF"; //hex colour values
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function start() {
  if(running) {
    bulbs.forEach(color => {
      color.style.background = getRandomColor();
    })
    setTimeout(start, 500);
    commentaries[1].style.opacity = 1;
  }
}

colorBtn.addEventListener('click', function() {
  colorBtn.innerText = 'STOP';
  if(running) {
    running = false;
    colorBtn.innerText = 'CHANGE COLOR';
  } else {
    running = true;
    start();
  }
})
